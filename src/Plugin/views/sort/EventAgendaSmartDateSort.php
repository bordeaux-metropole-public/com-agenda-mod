<?php

namespace Drupal\com_agenda_mod\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\Standard;

/**
 * Sort by date.
 *
 * @ViewsSort("event_agenda_smartdate_sort")
 */
class EventAgendaSmartDateSort extends Standard {

  /**
   * {@inheritdoc}
   */
  public function query() {

    $this->ensureMyTable();

    $field_name = "$this->tableAlias.$this->realField";

    $order = $this->options['order'];
    $reverse_order = ($order === 'DESC') ? 'ASC' : 'DESC';

    $period = $this->view->getExposedInput()['period'];
    switch ($period) {
      case 'past':
        $this->query->addOrderBy(
          NULL, NULL, $reverse_order, $field_name . '_value');
        break;

      default:
        $table_info = $this->view->query->getEntityTableInfo();
        $table_alias = $table_info['event_agenda']['alias'];
        $this->query->addOrderBy($table_alias, 'a_la_une', 'ASC');
        $this->query->addOrderBy(NULL, NULL, $order, $field_name . '_value');
    }
  }

}
