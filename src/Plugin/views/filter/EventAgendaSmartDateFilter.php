<?php

namespace Drupal\com_agenda_mod\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\StringFilter;

/**
 * Filter by start and end date.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("event_agenda_smartdate_filter")
 */
class EventAgendaSmartDateFilter extends StringFilter {

  /**
   * {@inheritdoc}
   */
  public function operatorOptions($which = 'title') {
    $options = [];
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
      // Only allow first operator which is "equals".
      break;
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {

    $this->ensureMyTable();

    $date_now = new \DateTime();
    $date_now->setTime(0, 0, 0);

    $field_name = "$this->tableAlias.$this->realField";

    $period = $this->value;
    switch ($period) {
      case 'week':
        $this->query->addWhere($this->options['group'], $field_name . "_end_value", $date_now->getTimestamp(), '>=');
        $date_now->modify('monday next week');
        $this->query->addWhere($this->options['group'], $field_name . "_value", $date_now->getTimestamp(), '<');
        break;

      case 'today':
        $this->query->addWhere($this->options['group'], $field_name . "_end_value", $date_now->getTimestamp(), '>=');
        $date_now->modify('tomorrow');
        $this->query->addWhere($this->options['group'], $field_name . "_value", $date_now->getTimestamp(), '<');
        break;

      case 'month':
        $this->query->addWhere($this->options['group'], $field_name . "_end_value", $date_now->getTimestamp(), '>=');
        $date_now->modify('first day of next month');
        $this->query->addWhere($this->options['group'], $field_name . "_value", $date_now->getTimestamp(), '<');
        break;

      case 'past':
        $this->query->addWhere($this->options['group'], $field_name . "_end_value", $date_now->getTimestamp(), '<');
        break;

      case 'All':
      case 'all':
      default:
        $this->query->addWhere($this->options['group'], $field_name . "_end_value", $date_now->getTimestamp(), '>=');
    }
  }

}
