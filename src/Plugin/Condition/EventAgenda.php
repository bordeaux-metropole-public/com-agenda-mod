<?php

  namespace Drupal\com_agenda_mod\Plugin\Condition;

  use Drupal\Core\Condition\Annotation\Condition;
  use Drupal\Core\Condition\ConditionPluginBase;
  use Drupal\Core\Entity\EntityStorageInterface;
  use Drupal\Core\Form\FormStateInterface;
  use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
  use Symfony\Component\DependencyInjection\ContainerInterface;

  /**
   * Provides a condition for articles marked as selected.
   *
   * This condition evaluates to TRUE when in a node context, and the node is
   * Article content type and the article was marked as selected article in a field.
   *
   * @Condition(
   *   id = "event_agenda",
   *   label = @Translation("Event Agenda"),

   * )
   *
   */
  class EventAgenda extends ConditionPluginBase implements ContainerFactoryPluginInterface {

    /**
     * The entity storage.
     *
     * @var \Drupal\Core\Entity\EntityStorageInterface
     */
    protected $entityStorageEventType;

    /**
     * The entity storage.
     *
     * @var \Drupal\Core\Entity\EntityStorageInterface
     */
    protected $entityStorageNodeType;

    /**
     * Creates a new SelectedArticle instance.
     *
     * @param array $configuration
     *   The plugin configuration.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     */
    public function __construct(EntityStorageInterface $entity_storage_event_type,EntityStorageInterface $entity_storage_node_type,array $configuration, $plugin_id, $plugin_definition) {
      parent::__construct($configuration, $plugin_id, $plugin_definition);
      $this->entityStorageEventType = $entity_storage_event_type;
      $this->entityStorageNodeType = $entity_storage_node_type;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
      return new static(
        $container->get('entity_type.manager')->getStorage('event_type'),
        $container->get('entity_type.manager')->getStorage('node_type'),
        $configuration,
        $plugin_id,
        $plugin_definition
      );
    }

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() {
      // This default value will mark the block as hidden.
      return [];
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
      $form = parent::buildConfigurationForm($form, $form_state);
      unset($form['negate']);

      $form['show_event_agenda'] = [
        '#title' => $this->t('Display for all EventAgenda'),
        '#type' => 'checkbox',
        '#default_value' => $this->configuration['show_event_agenda'],
      ];
      // List type event type available
      $options = [];
      $node_types = $this->entityStorageEventType->loadMultiple();
      foreach ($node_types as $type) {
        $options[$type->id()] = $type->label();
      }
      $form['show_event_type'] = [
        '#title' => $this->t('Display only in Selected Event Type'),
        '#type' => 'checkboxes',
        '#options' => $options,
        '#default_value' => $this->configuration['show_event_type'],
      ];
      $form['show_node'] = [
        '#title' => $this->t('Display for all Node'),
        '#type' => 'checkbox',
        '#default_value' => $this->configuration['show_node'],
      ];
      // List all node type available
      $node_types = $this->entityStorageNodeType->loadMultiple();
      foreach ($node_types as $type) {
        $options[$type->id()] = $type->label();
      }
      $form['show_node_type'] = [
        '#title' => $this->t('Display only in Selected Node Type'),
        '#type' => 'checkboxes',
        '#options' => $options,
        '#default_value' => $this->configuration['show_node_type'],
      ];
      return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
      if (!$this->checkArrayActive($form_state->getValue('show_event_type'))
          && !$form_state->getValue('show_event_agenda')
        && !$form_state->getValue('show_node')
          && !$this->checkArrayActive($form_state->getValue('show_node_type'))) {
        unset($this->configuration['show_event_agenda']);
        unset($this->configuration['show_event_type']);
        unset($this->configuration['show_node']);
        unset($this->configuration['show_node_type']);
        if($this->configuration['context_mapping'])
        {
          unset($this->configuration['context_mapping']);
        }
        if($this->configuration['negate'])
        {
          unset($this->configuration['negate']);
        }
      }
      else
      {
        $this->configuration['show_event_agenda'] = $form_state->getValue('show_event_agenda');
        $this->configuration['show_event_type'] = $form_state->getValue('show_event_type');
        $this->configuration['show_node'] = $form_state->getValue('show_node');
        $this->configuration['show_node_type'] = $form_state->getValue('show_node_type');
      }
    }

    private function checkArrayActive($array) {
      foreach ($array as $element) {
        if ($element) {
          return true;
        }
      }
      return false;
    }

    /**
     * {@inheritdoc}
     */
    public function summary() {

      if (count($this->configuration['show_event_type']) > 1) {
        $bundles = $this->configuration['show_event_type'];
        $last = array_pop($bundles);
        $bundles = implode(', ', $bundles);
      }
      if (count($this->configuration['show_node_type']) > 1) {
        $bundles = $this->configuration['show_node_type'];
        $last = array_pop($bundles);
        $bundles = implode(', ', $bundles);
      }
      if(count($this->configuration['show_event_type']) > 1 && count($this->configuration['show_node_type']) > 1) {
        return $this->t('The node bundle is @bundles or @last', ['@bundles' => $bundles, '@last' => $last]);
      }
      // The condition is not enabled.
      return $this->t('The block will be shown in all pages.');
    }

    /**
     * {@inheritdoc}
     */
    public function evaluate() {
      // Check if event-type ou node-type is checked
      if (empty($this->configuration['show_event_agenda']) && empty($this->configuration['show_node_type'])
        && empty($this->configuration['show_node']) && empty($this->configuration['show_node_type'])) {
        return TRUE;
      }
      $event_agenda = \Drupal::routeMatch()->getParameter('event_agenda');
      if ($event_agenda && (
          $this->configuration['show_event_agenda']
          || isset($this->configuration['show_event_type'][$event_agenda->getEventTypeKey()]) && $this->configuration['show_event_type'][$event_agenda->getEventTypeKey()]
        )
      ) {
        return TRUE;
      }
      $node = \Drupal::routeMatch()->getParameter('node');
      if ($node && (
          $this->configuration['show_node']
          || isset($this->configuration['show_node_type'][$node->getType()]) && $this->configuration['show_node_type'][$node->getType()]
        )
      ) {
        return TRUE;
      }
      return FALSE;
    }
  }

