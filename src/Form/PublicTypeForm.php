<?php

namespace Drupal\com_agenda_mod\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PublicTypeForm.
 */
class PublicTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $public_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $public_type->label(),
      '#description' => $this->t("Label for the Public type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $public_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\com_agenda_mod\Entity\PublicType::load',
      ],
      '#disabled' => !$public_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $public_type = $this->entity;
    $status = $public_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Public type.', [
          '%label' => $public_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Public type.', [
          '%label' => $public_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($public_type->toUrl('collection'));
  }

}
