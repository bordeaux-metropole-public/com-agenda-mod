<?php

  namespace Drupal\com_agenda_mod\Form;

  use Drupal\Core\Entity\ContentEntityForm;
  use Drupal\Core\Entity\EntityForm;
  use Drupal\Core\Form\FormBase;
  use Drupal\Core\Form\FormStateInterface;
  use Symfony\Component\DependencyInjection\ContainerInterface;

  /**
   * Form controller for Event agenda edit forms.
   *
   * @ingroup com_agenda_mod
   */
  class EventAgendaFilterForm extends FormBase {

    public function getFormId()
    {
      return 'event_agenda_filter_collection_form';
    }

    private function keyFilter() {
      return [
        'event_type' => [0=>'event_type', 1=>'Event Type'],
        'public_type' => [0=>'public_type',  1=>'Public Type'],
        'localisation_type' => [0=>'localisation_type', 1=>'Localisation Type'],
        'handicap_type' => [0=>'handicap_type', 1=>'Handicap Type'],
      ];
    }

    public function buildForm(array $form, FormStateInterface $form_state) {
      $request = \Drupal::request();

      $form['filter'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['form--inline', 'clearfix'],
        ],
      ];

      $form['filter']['name'] = [
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#default_value' => $request->get('name') ?? '',
      ];

      foreach ($this->keyFilter() as $key => $item) {
        $form['filter'][$key] = $this->getFilterEntity($request, $key, $item[0], $item[1]);
      }

      $form['filter']['status'] = [
        '#type' => 'select',
        '#title' => t('Status'),
        '#options' =>     ['publie' => 'Publié', 'non-publie' => 'Non Publié'],
        '#default_value' => $request->get('status') == 'non-publie' ? 'non-publie' : 'publie',
      ];

      $form['actions']['wrapper'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['form-item']],
      ];

      $form['actions']['wrapper']['submit'] = [
        '#type' => 'submit',
        '#value' => 'Filter',
      ];

      if ($request->getQueryString()) {
        $form['actions']['wrapper']['reset'] = [
          '#type' => 'submit',
          '#value' => 'Reset',
          '#submit' => ['::resetForm'],
        ];
      }

      return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
      $query = [];

      foreach ($this->keyFilter() as $key => $item) {
        $value = $form_state->getValue($key) ?? 0;
        if ($value) {
          $query[$key] = $value;
        }
      }

      $value = $form_state->getValue('name') ?? '';
      if ($value) {
        $query['name'] = $value;
      }

      $value = $form_state->getValue('status') == 'non-publie' ? 'non-publie' : 'publie';
      if ($value) {
        $query['status'] = $value;
      }

      $form_state->setRedirect('entity.event_agenda.collection', $query);
    }

    public function resetForm(array $form, FormStateInterface &$form_state) {
      $form_state->setRedirect('entity.event_agenda.collection');
    }

    private function getFilterEntity($request, $filter, $entity_type, $label) {
      $type = [
        '' => '-- All --'
      ];
      foreach(\Drupal::entityTypeManager()->getStorage($entity_type)->loadMultiple() as $key => $item) {
        $type[$key] = $item->label();
      }
      return [
        '#type' => 'select',
        '#title' => t($label),
        '#options' =>     $type,
        '#default_value' => $request->get($filter) ?? 0,
      ];
    }

  }
