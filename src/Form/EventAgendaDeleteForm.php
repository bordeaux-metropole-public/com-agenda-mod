<?php

namespace Drupal\com_agenda_mod\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Event agenda entities.
 *
 * @ingroup com_agenda_mod
 */
class EventAgendaDeleteForm extends ContentEntityDeleteForm {


}
