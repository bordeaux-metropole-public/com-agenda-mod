<?php

namespace Drupal\com_agenda_mod\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class HandicapTypeForm.
 */
class HandicapTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $handicap_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $handicap_type->label(),
      '#description' => $this->t("Label for the Handicap type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $handicap_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\com_agenda_mod\Entity\HandicapType::load',
      ],
      '#disabled' => !$handicap_type->isNew(),
    ];

    $form['image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image'),
      '#description' => $this->t('Upload an image for this handicap type.'),
      '#upload_location' => 'public://handicap_type_images/',
      '#default_value' => $handicap_type->id($handicap_type->get('image')),
      '#required' => FALSE,
    ];
    if ($handicap_type->get('image') != NULL) {
      $image = $handicap_type->get('image');
      if ($image && !is_array($image)) {
        $file_url_generator = \Drupal::service('file_url_generator');
        $image_url = $file_url_generator->generateString($image);
        $form['image_preview'] = [
          '#type' => 'markup',
          '#markup' => '<div><strong>' . $this->t('Current Image:') . '</strong></div>' .
          '<img src="' . $image_url . '" alt="' . $handicap_type->label() . '" style="max-width:200px; max-height:200px;">',
        ];
      }
    }

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $handicap_type = $this->entity;
    $image = $form_state->getValue('image');
    if (!empty($image)) {
      $file = \Drupal::entityTypeManager()->getStorage('file')->load($image[0]);
      $file->setPermanent();
      $file->save();
      $this->entity->setImage($file->getFileUri());
    }
    else {
      $this->entity->setImage(NULL);
    }
    $status = $handicap_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Handicap type.', [
          '%label' => $handicap_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Handicap type.', [
          '%label' => $handicap_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($handicap_type->toUrl('collection'));
  }

}
