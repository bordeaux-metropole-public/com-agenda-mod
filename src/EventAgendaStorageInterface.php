<?php

namespace Drupal\com_agenda_mod;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\com_agenda_mod\Entity\EventAgendaInterface;

/**
 * Defines the storage handler class for Event agenda entities.
 *
 * This extends the base storage class, adding required special handling for
 * Event agenda entities.
 *
 * @ingroup com_agenda_mod
 */
interface EventAgendaStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Event agenda revision IDs for a specific Event agenda.
   *
   * @param \Drupal\com_agenda_mod\Entity\EventAgendaInterface $entity
   *   The Event agenda entity.
   *
   * @return int[]
   *   Event agenda revision IDs (in ascending order).
   */
  public function revisionIds(EventAgendaInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Event agenda author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Event agenda revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\com_agenda_mod\Entity\EventAgendaInterface $entity
   *   The Event agenda entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(EventAgendaInterface $entity);

  /**
   * Unsets the language for all Event agenda with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
