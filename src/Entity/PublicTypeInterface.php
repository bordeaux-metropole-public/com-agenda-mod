<?php

namespace Drupal\com_agenda_mod\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Public type entities.
 */
interface PublicTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
