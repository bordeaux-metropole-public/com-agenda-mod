<?php

namespace Drupal\com_agenda_mod\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Public type entity.
 *
 * @ConfigEntityType(
 *   id = "public_type",
 *   label = @Translation("Public type"),
 *   handlers = {
 *     "access" = "Drupal\com_agenda_mod\PublicTypeAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\com_agenda_mod\PublicTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\com_agenda_mod\Form\PublicTypeForm",
 *       "edit" = "Drupal\com_agenda_mod\Form\PublicTypeForm",
 *       "delete" = "Drupal\com_agenda_mod\Form\PublicTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\com_agenda_mod\PublicTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "public_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 *   admin_permission = "administer public type entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/public_type/{public_type}",
 *     "add-form" = "/admin/structure/public_type/add",
 *     "edit-form" = "/admin/structure/public_type/{public_type}/edit",
 *     "delete-form" = "/admin/structure/public_type/{public_type}/delete",
 *     "collection" = "/admin/structure/public_type"
 *   }
 * )
 */
class PublicType extends ConfigEntityBase implements PublicTypeInterface {

  /**
   * The Public type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Public type label.
   *
   * @var string
   */
  protected $label;

  /**
   * @return string
   */
  public function toString() {
    return $this->label;
  }
}
