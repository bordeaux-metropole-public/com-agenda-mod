<?php

namespace Drupal\com_agenda_mod\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Localisation type entities.
 */
interface LocalisationTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
