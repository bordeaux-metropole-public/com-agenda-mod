<?php

namespace Drupal\com_agenda_mod\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Event type entity.
 *
 * @ConfigEntityType(
 *   id = "event_type",
 *   label = @Translation("Event type"),
 *   handlers = {
 *     "access" = "Drupal\com_agenda_mod\EventTypeAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\com_agenda_mod\EventTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\com_agenda_mod\Form\EventTypeForm",
 *       "edit" = "Drupal\com_agenda_mod\Form\EventTypeForm",
 *       "delete" = "Drupal\com_agenda_mod\Form\EventTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\com_agenda_mod\EventTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "event_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 *   admin_permission = "administer event type entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/event_type/{event_type}",
 *     "add-form" = "/admin/structure/event_type/add",
 *     "edit-form" = "/admin/structure/event_type/{event_type}/edit",
 *     "delete-form" = "/admin/structure/event_type/{event_type}/delete",
 *     "collection" = "/admin/structure/event_type"
 *   }
 * )
 */
class EventType extends ConfigEntityBase implements EventTypeInterface {

  /**
   * The Event type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Event type label.
   *
   * @var string
   */
  protected $label;

  /**
   * @return string
   */
  public function toString() {
    return $this->label;
  }
}
