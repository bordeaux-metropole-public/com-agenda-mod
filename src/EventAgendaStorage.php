<?php

namespace Drupal\com_agenda_mod;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\com_agenda_mod\Entity\EventAgendaInterface;

/**
 * Defines the storage handler class for Event agenda entities.
 *
 * This extends the base storage class, adding required special handling for
 * Event agenda entities.
 *
 * @ingroup com_agenda_mod
 */
class EventAgendaStorage extends SqlContentEntityStorage implements EventAgendaStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(EventAgendaInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {event_agenda_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {event_agenda_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(EventAgendaInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {event_agenda_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('event_agenda_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
