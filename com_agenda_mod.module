<?php

/**
 * @file
 * Contains com_agenda_mod.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_help().
 */
function com_agenda_mod_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the com_agenda_mod module.
    case 'help.page.com_agenda_mod':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('My Awesome Module') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function com_agenda_mod_theme() {

  $theme = [];
  $theme['com_agenda_mod'] = [
    'render element' => 'children',
  ];
  $theme['event_agenda'] = [
    'render element' => 'elements',
    'file' => 'event_agenda.page.inc',
    'template' => 'event_agenda',
  ];
  $theme['event_agenda_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'event_agenda.page.inc',
  ];
  return $theme;
}

  /**
   * Implements hook_theme_suggestions_HOOK().
   */
  function com_agenda_mod_theme_suggestions_event_agenda(array $variables) {
    $suggestions = [];
    $entity = $variables['elements']['#event_agenda'];
    $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

    $suggestions[] = 'event_agenda__' . $sanitized_view_mode;
    $suggestions[] = 'event_agenda__' . $entity->bundle();
    $suggestions[] = 'event_agenda__' . $entity->bundle() . '__' . $sanitized_view_mode;
    $suggestions[] = 'event_agenda__' . $entity->id();
    $suggestions[] = 'event_agenda__' . $entity->id() . '__' . $sanitized_view_mode;
    $suggestions[] = 'event_agenda__' . $entity->getEventTypeKey();
    $suggestions[] = 'event_agenda__' . $entity->getEventTypeKey() . '__' . $sanitized_view_mode;
    return $suggestions;
  }

/**
 * Implements hook_field_views_data_alter().
 *
 * Views integration for smartdate fields in event_agenda entities.
 * Adds a custom filter and a custom sort.
 *
 * @see views_field_default_views_data()
 */
function com_agenda_mod_field_views_data_alter(
  array &$data,
  FieldStorageConfigInterface $field_storage) {

  if ($field_storage->getType() == 'smartdate'
    && $field_storage->getTargetEntityTypeId() == 'event_agenda') {

    foreach ($data as $table_name => $table_data) {

      $field_name = $field_storage->getName();
      $field_label = $table_data[$field_storage->getName()]['title'];

      $data[$table_name][$field_name . '_filter'] = [
        'title' => t('SmartDate Filter: @label', ['@label' => $field_label]),
        'group' => t('Event agenda'),
        'filter' => [
          'title' => t('SmartDate Filter (@name)', ['@name' => $field_name]),
          'field' => $field_name,
          'id' => 'event_agenda_smartdate_filter',
        ],
      ];

      $data[$table_name][$field_name . '_sort'] = [
        'title' => t('SmartDate Sort: @label', ['@label' => $field_label]),
        'group' => t('Event agenda'),
        'sort' => [
          'title' => t('SmartDate Sort (@name)', ['@name' => $field_name]),
          'field' => $field_name,
          'id' => 'event_agenda_smartdate_sort',
        ],
      ];
    }

  }

}

